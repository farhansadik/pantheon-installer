# Pantheon Desktop for Arch Linux 

A easy way to install Pantheon Desktop on Arch Linux

<p align="center">
  <img src="images/elementary_logo.svg" alt="Alt text" width="200" height="200">
</p>


[elementary OS](https://elementary.io/) releases are derived from [Ubuntu's](https://wiki.archlinux.org/index.php/Arch_compared_to_other_distributions#Ubuntu) LTS releases, typically trailing Ubuntu's cycle by a few weeks or  months. However, its constituent packages are updated continuously. See  the official [github repository](https://github.com/elementary) and consult their [community slack](https://elementarycommunity.slack.com/).


## Installation 
1. Clone Repository 
```
git clone https://gitlab.com/farhansadik/pantheon-installer.git 
```
2. Run Installer <br>
> Do not run as **root**. 
``` 
./install
```

## LightDM
If you need lightdm you can install using those command, but Pantheon also support on gdm. Lightdm is recommended. 
```
# pacman -S lightdm lightdm-pantheon-greeter 
# systemctl enable lightdm.service
# systemctl start lightm.service
```

## Notes 
* If you've seen any dependency missing then install it manually. 
* If you've got `pantheon-dock-git and plank are in conflict. Remove plank? [y/N] ` conflict message, then just press **y** or 

## Uninstall 
You can uninstall pantheon but executing `uninstall.bash` file. But this script is not recommended. It could damage your system. It would be better to uninstall those packages manually.

## List of Aur Packages
* [pantheon-dock-git](https://aur.archlinux.org/packages/pantheon-dock-git)

## Tweaks

 * **How to change pantheon terminal font**

	Open `dconf-editor`. Go to `io.elementary.terminal.settings.font`. Enter your preffered font name on '**custom value**' option. Just enter as `'DejaVu Sans Mono Book 12'` without quotation.
	```
	Font Name : DejaVu Sans Mono Book
	Font Size : 12 
	```

## Screenshot

![screen_1](images/desktop.png)

![screen_3](images/file_manager.PNG)

![screen_2](images/sysinfo.png)


## Theme and Icon 
* [Theme](https://github.com/vinceliuice/WhiteSur-gtk-theme)
* [Icon](https://github.com/vinceliuice/WhiteSur-icon-theme)

## Sources
1. [Arch Wiki](https://wiki.archlinux.org/title/Pantheon)
2. [Pantheon Group](https://archlinux.org/groups/x86_64/pantheon/) 

## Contribution

<a href="https://gitlab.com/lapato">
  <img src="https://gitlab.com/uploads/-/system/user/avatar/5121923/avatar.png?width=400" alt="alt" width="80" height="85">
</a>
King Bob

## Farhan Sadik
